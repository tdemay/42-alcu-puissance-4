/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ia.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdemay <tdemay@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 14:18:00 by tdemay            #+#    #+#             */
/*   Updated: 2014/03/09 14:18:00 by tdemay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

int		place_line_H(t_env *e, int i, int j, int recon)
{
	while (recon--)
	{
		if (e->game[i][j] == 0)
			return (j + 1);
		j++;
	}
	return (0);
}

int		ia_line_H(t_env *e, int player, int adv, int recon)
{
	int		i;
	int		j;
	int		cnt;

	i = 0;
	while (i < e->l && !(j = 0))
	{
		while (j <= (e->c - 4) && !(cnt = 0))
		{
				cnt += (e->game[i][j] == adv)? 1: 0;
				cnt += (e->game[i][j + 1] == adv)? 1: 0;
				cnt += (e->game[i][j + 2] == adv)? 1: 0;
				cnt += (e->game[i][j + 3] == adv)? 1: 0;
				if (recon == cnt && place_line_H(e, i, j, recon))
					return (place_line_H(e, i, j, recon));
			j++;
		}
		i++;
	}
	return (0);
}

int		ia_line_V(t_env *e, int player, int adv, int recon)
{
	int		i;
	int		j;
	int		cnt;

	i = 0;
	while (i <= (e->l - 4)  && !(j = 0))
	{
		while (e->game[i][j] != player && j < e->c && !(cnt = 0))
		{
				cnt += (e->game[i][j] == adv)? 1: 0;
				cnt += (e->game[i + 1][j] == adv)? 1: 0;
				cnt += (e->game[i + 2][j] == adv)? 1: 0;
				cnt += (e->game[i + 3][j] == adv)? 1: 0;
				if (recon == cnt && e->game[0][j] == 0)
					return (j + 1);
				j++;
		}
		i++;
	}
	return (0);
}

int		ia_point(t_env *e, int player, int adv, int cnt)
{
	int		i;
	int		j;
	int		tmp;
	int		ret;

	i = ret = 0;
	while (i < e->l && !(j = 0))
	{
		while (j < e->c && e->game[i][j] == 0 && !(tmp = 0))
		{
			tmp += (i > 0 && j > 0 && e->game[i - 1][j - 1] == adv)? 1: 0;
			tmp += (i < (e->l - 1) && e->game[i + 1][j] == adv)? 1: 0;
			tmp += (i < (e->l - 1) && j < (e->c - 1) && e->game[i + 1][j + 1] == adv)? 1: 0;
			tmp += (i < (e->l - 1) && j > 0 && e->game[i + 1][j - 1] == adv)? 1: 0;
			tmp += (j < (e->c - 1) && e->game[i][j + 1] == adv)? 1: 0;
			tmp += (i > 0 && j < (e->c - 1) && e->game[i - 1][j + 1] == adv)? 1: 0;
			tmp += (j > 0 && e->game[i][j - 1] == adv)? 1: 0;
			tmp += (i > 0 && e->game[i - 1][j] == adv)? 1: 0;
			if (cnt <= tmp && (ret = j + 1))
				cnt = tmp;
			j++;
		}
		i++;
	}
	return (ret);
}

int		place_place(t_env *e, int player, int adv)
{
	int		ret;
	int		i;
	int		j;

	i = 0;
	if ((ret = ia_line_V(e, player, player, 2)) && ft_putendl("F"))
		return (ret);
	else if ((ret = ia_line_H(e, player, player, 2)) && ft_putendl("G"))
		return (ret);
	else if ((ret = ia_line_V(e, player, adv, 2)) && ft_putendl("H"))
		return (ret);
	else if ((ret = ia_line_H(e, player, adv, 2)) && ft_putendl("I"))
		return (ret);
	ft_putendl("J");
	ret = ((e->c / 2) + (e->c % 2));
	if (e->game[0][ret - 1] == 0 && ft_putendl("K"))
		return (ret);
	return (ia_point(e, player, player, 0));
}

int		try_move_IA(t_env *e, int player, int adv)
{
	int		ret;

	ret = 0;
	if ((ret = ia_line_V(e, player, player, 3)) && ft_putendl("A"))
		return (ret);
	else if ((ret = ia_line_H(e, player, player, 3)) && ft_putendl("B"))
		return (ret);
	else if ((ret = ia_line_V(e, player, adv, 3)) && ft_putendl("C"))
		return (ret);
	else if ((ret = ia_line_H(e, player, adv, 3)) && ft_putendl("D"))
		return (ret);
	else if (ft_putendl("E"))
		return (place_place(e, player, adv));

	return (ret);
}