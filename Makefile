# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tdemay <tdemay@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/03/08 14:04:26 by tdemay            #+#    #+#              #
#    Updated: 2014/03/08 14:04:26 by tdemay           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: clean fclean re

NAME = puissance4

FLAGS = -Wall -Wextra -Werror

LIB = -O3 -Llibft/ -lft

SRC = game.c check.c ia.c

INC = puissance4.h

OBJ = $(SRC:.c=.o)

.SILENT:

all: $(NAME)

$(NAME): $(OBJ) $(INC) main.c ./libft/libft.a
	printf "\r\033[38;5;11m⌛  MAKE   $(NAME) plz wait ...\033[K\033[0m"
	cc $(FLAGS) $(LIB) $(OBJ) main.c -o $(NAME)
	echo -en "\r\033[38;5;22m✅  MAKE   $(NAME)\033[K\033[0m"

%.o: %.c %.h
	cc $(FLAGS) -c $< -o $@

./libft/libft.a:
	make -C libft/ fclean
	make -C libft/

clean:
	printf "\r\033[38;5;25m⌛  CLEAN  $(NAME) plz wait ...\033[K\033[0m"
	rm -f $(OBJ)
	echo -en "\r\033[38;5;124m❌  CLEAN  $(NAME)\033[K\033[0m"

fclean: clean
	printf "\r\033[38;5;25m⌛  FCLEAN $(NAME) plz wait ...\033[K\033[0m"
	rm -f $(NAME)
	make -C ./libft/ fclean
	echo -en "\r\033[38;5;124m❌  FCLEAN $(NAME)\033[K\033[0m"

re: fclean all