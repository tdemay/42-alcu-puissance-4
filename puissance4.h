/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   puissance4.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdemay <tdemay@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 14:59:13 by tdemay            #+#    #+#             */
/*   Updated: 2014/03/09 14:59:13 by tdemay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef ALCU_H
# define ALCU_H

# include <time.h>
# include "libft/libft.h"

# define ISIA 1
# define ISRP 0
# define P1 1
# define P2 2

typedef struct	s_env
{
	int		p1;
	int		p2;
	int		**game;
	int		l;
	int		c;
	int		win;
	int		cnt;
}				t_env;

int		play_player(t_env *e, int ia, int player, int adv);
void	test_win(t_env *e, int player, int ia);
void	aff_game(t_env *e);
int		try_move_IA(t_env *e, int player, int adv);

#endif /* !ALCU_H */