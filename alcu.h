/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alcu.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdemay <tdemay@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 15:11:40 by tdemay            #+#    #+#             */
/*   Updated: 2014/03/08 15:11:40 by tdemay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ALCU_H
# define ALCU_H

# include <time.h>
# include "libft/libft.h"

# define ISIA 1
# define ISRP 0
# define P1 1
# define P2 2

typedef struct	s_env
{
	int		p1;
	int		p2;
	int		**game;
	int		lines;
	int		columns;
	int		win;
	int		cnt;
}				t_env;

int		play_player(t_env *e, int ia, int player);

#endif /* !ALCU_H */