/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdemay <tdemay@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 15:10:46 by tdemay            #+#    #+#             */
/*   Updated: 2014/03/08 17:51:20 by tdemay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

int		**gen_grille(int l, int c, t_env *e)
{
	int		**ret;

	ret = NULL;
	if (l >= 6 && c >= 7)
	{
		e->l = l;
		e->c = c;
		ret = (int**)malloc(l * sizeof(int*));
		while (l--)
		{
			ret[l] = (int*)malloc(c * sizeof(int));
		}
	}
	else
	{
		ft_putendl("Usage : ./puissance4 <lines> <Columns>");
		ft_putendl("Puissance4 : min size for lines is 6 and 7 for columns");
	}
	return (ret);
}

void	aff_game(t_env *e)
{
	register unsigned int		i;
	register unsigned int		j;

	i = 0;
	while (i < (unsigned int)e->l)
	{
		j = 0;
		while (j < (unsigned int)e->c)
		{
			ft_putstr("\033[48;5;18m");
			if (e->game[i][j]== 1)
				ft_putstr("\033[38;5;226m⬤");
			else if (e->game[i][j]  == 2)
				ft_putstr("\033[38;5;196m⬤");
			else
				ft_putstr("\033[38;5;8m⬤");
			ft_putchar(' ');
			ft_putstr("\033[0m");
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}


void	menu(t_env *e)
{
	char	*ligne;

	ft_putendl("###################################################");
	ft_putendl("#                                                 #");
	ft_putendl("#                   Puissance 4                   #");
	ft_putendl("#                                                 #");
	ft_putendl("###################################################\n");
	ft_putendl("    Real Player (1)              IA (2)\n");
	ft_putstr("Player 1 is : ");
	while (get_next_line(0, &ligne))
	{
		if (ft_atoi(ligne) == 1 && !(e->p1 = ISRP))
		{
			free(ligne);
			break ;
		}
		else if(ft_atoi(ligne) == 2 && (e->p1 = ISIA))
		{
			free(ligne);
			break ;
		}
		free(ligne);
	}
	ft_putstr("Player 2 is : ");
	while (get_next_line(0, &ligne))
	{
		if (ft_atoi(ligne) == 1 && !(e->p2 = ISRP))
		{
			free(ligne);
			break ;
		}
		else if(ft_atoi(ligne) == 2 && (e->p2 = ISIA))
		{
			free(ligne);
			break ;
		}
		free(ligne);
	}
	ft_putendl("\n\nGame Start !!!\n");
}

int		main(int ac, char **av)
{
	t_env	e;

	if (ac == 3 && !(e.cnt = e.win = 0))
	{
		if (!(e.game = gen_grille(ft_atoi(av[1]), ft_atoi(av[2]), &e)))
			return (0);
		srand(time(NULL));
		menu(&e);
		e.p1 = ISRP;
		e.p2 = ISIA;
		e.cnt = rand() % 2;
		while (!e.win)
		{
			aff_game(&e);
			if (e.cnt % 2)
				test_win(&e, play_player(&e, e.p1, P1, P2), e.p1);
			else
				test_win(&e, play_player(&e, e.p2, P2, P1), e.p2);
			e.cnt = ((e.cnt % 2) + 1);
		}
	}
	else
		ft_putendl("Usage : ./puissance4 <lines> <Columns>");
	return (0);
}