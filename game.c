/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdemay <tdemay@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 17:52:02 by tdemay            #+#    #+#             */
/*   Updated: 2014/03/08 17:52:02 by tdemay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

int		play_IA(t_env *e, int player, int adv)
{
	int		ret;

	ret = try_move_IA(e, player, adv);
	ft_putstr("IA's move :");
	ft_putnbr(ret);
	ft_putendl("");
	return (ret);
}

int		place_coin(t_env *e, int col, int piece)
{
	int		i;

	(void) piece;
	(void) col;

	i = e->l;
	while (i)
	{
		if (e->game[i - 1][col - 1] == 0)
		{
			e->game[i - 1][col - 1] = piece;
			return (0);
		}
		i--;
	}
	return (1);
}

int		play_player(t_env *e, int ia, int player, int adv)
{
	char	*ligne;
	int		error;

	if (ia)
		place_coin(e, play_IA(e, player, adv), player);
	else if ((error = 1))
	{
		ft_putstr("Player ");
		ft_putnbr(player);
		ft_putstr(", your move : ");
		while (get_next_line(0, &ligne))
		{
			if (ft_atoi(ligne) <= e->c && ft_atoi(ligne) > 0)
				error = place_coin(e, ft_atoi(ligne), player);
			free(ligne);
			if (error)
				ft_putendl("Incorrect value. No columns");
			else
				break ;
		}
	}
	ft_putendl("");
	return (player);
}