/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdemay <tdemay@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 11:24:07 by tdemay            #+#    #+#             */
/*   Updated: 2014/03/09 11:24:07 by tdemay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

int		check_line_H(t_env *e, int player)
{
	int		i;
	int		j;

	i = 0;
	while (i <= (e->lines - 4) && !(j = 0))
	{
		while (j < e->columns)
		{
			if (e->game[i][j] == player && e->game[i + 1][j] == player &&
				e->game[i + 2][j] == player && e->game[i + 3][j] == player)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int		check_line_V(t_env *e, int player)
{
	int		i;
	int		j;

	i = 0;
	while (i < e->lines && !(j = 0))
	{
		while (j <= (e->columns - 4))
		{
			if (e->game[i][j] == player && e->game[i][j + 1] == player &&
				e->game[i][j + 2] == player && e->game[i][j + 3] == player)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}


int		check_diag_BH(t_env *e, int play)
{
	int		i;
	int		j;

	i = 3;
	while (i < e->lines && !(j = 0))
	{
		while (j <= (e->columns - 4))
		{
			if (e->game[i][j] == play && e->game[i - 1][j + 1] == play &&
				e->game[i - 2][j + 2] == play && e->game[i - 3][j + 3] == play)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}


int		check_diag_HB(t_env *e, int play)
{
	int		i;
	int		j;

	i = 0;
	while (i <= (e->lines - 4) && !(j = 0))
	{
		while (j <= (e->columns - 4))
		{
			if (e->game[i][j] == play && e->game[i + 1][j + 1] == play &&
				e->game[i + 2][j + 2] == play && e->game[i + 3][j + 3] == play)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

void	test_win(t_env *e, int player, int ia)
{
	if (check_line_H(e, player) || check_line_V(e, player) ||
		check_diag_BH(e, player) || check_diag_HB(e, player))
	{
		e->win = 1;
		aff_game(e);
		ft_putstr("Player ");
		ft_putnbr(player);
		if (ia)
			ft_putstr(" (IA)");
		if (player == 1)
				ft_putendl(" win with \033[38;5;226m⬤\033[0m  coin.");
		else if (player  == 2)
				ft_putendl(" win with \033[38;5;196m⬤\033[0m  coin.");
	}
}